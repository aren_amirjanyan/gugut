const Sequelize = require('sequelize'),
  bcrypt = require("bcrypt"),
  {gtoken} = require('./gtoken');

/**
 *
 * @param config
 * @returns {{loadModel: (function(*, *=): any), sequelize: (function(): *), close: close, connect: connect}}
 */
const gorm = (config) => {
  return {
    /**
     * Get Sequelize instance
     */
    sequelize: () => {
      return new Sequelize(config.database, config.username, config.password, {
        host: config.host,
        dialect: config.dialect,
        pool: config.pool
      })
    },
    /**
     * Connect DB with sequelize config
     * @param sequelize
     */
    connection: (sequelize) => {
      sequelize.authenticate()
      .then(() => {
        console.log('Connection has been established successfully.');
      })
      .catch(err => {
        console.error('Unable to connect to the database:', err);
      });
    },
    /**
     * Close sequelize connection
     * @param sequelize
     */
    close: (sequelize) => {
      sequelize.close();
    },
    /**
     * Load model with sequelize
     * @param modelName
     * @param sequelize
     * @returns {any}
     */
    loadModel: (modelName, sequelize) => {
      return require(`./models/${modelName}`)(sequelize, Sequelize);
    },
  }
};

/**
 *
 * @param config
 * @param sequelize
 * @returns {{signIn: (function(*, *=): Promise<T | T>), profile: (function(*=, *=): Promise<T | T>), signUp: (function(*=): Promise<T | {data: null, success: boolean, error: boolean, message: *}>)}}
 */
const gauth = (config, sequelize) => {
  
  const userModel = gorm(config).loadModel('User', sequelize);
  
  return {
    /**
     * Get expire token after sign in with email and password
     * @param email
     * @param password
     * @returns {Promise<T>}
     */
    signIn: (email, password) => {
      // find with email address
      return userModel.findOne({
        where: {
          email
        }
      }).then(user => {
        //check user is exist
        if (user) {
          if (bcrypt.compareSync(password, user.password)) {
            // user will be the first entry of the users table with the email and password
            const userData = {
              first_name: user.first_name,
              last_name: user.last_name,
              email: user.email,
            };
            const token = gtoken(config.gtoken).encrypt(userData);
            // save access token in user table
            user.access_token = token;
            user.save();
            return {
              data: token,
              error: false,
              success: true,
              message: ''
            };
          }
          else {
            return {
              data: null,
              error: true,
              success: false,
              message: "Password incorrect!"
            };
          }
        }
        else {
          return {
            data: null,
            error: true,
            success: false,
            message: "User not found!"
          };
        }
      }).catch(error => error);
    },
    /**
     * Get user data after sign up
     * @param data
     * @returns {Promise<T | {data: null, success: boolean, error: boolean, message: *}>}
     */
    signUp: (data) => {
      return userModel.sync().then(() => {
        return userModel.findOne({
          where: {
            email: data.email
          }
        }).then(async user => {
          if (!user) {
            const user = await userModel.create(data);
            return {
              data: user,
              error: false,
              success: true,
              message: 'User successfully created!'
            };
          }
          else {
            return {
              data: null,
              error: true,
              success: false,
              message: "User with email address already exists"
            };
          }
        }).catch(error => {
          return {
            data: null,
            error: true,
            success: false,
            message: error.message
          };
        });
      }).catch(error => {
        return {
          data: null,
          error: true,
          success: false,
          message: error.message
        };
      });
    },
    /**
     * Get user data with user email and token
     * @param email
     * @param token
     * @returns {Promise<T>}
     */
    profile: (email, token) => {
      return userModel.findOne({
        where: {
          email: email
        },
        attributes: ['id', 'first_name', 'last_name', 'email']
      }).then(user => {
        if (gtoken(config.gtoken).verify(token, {
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
        })) {
          return {
            data: user,
            error: false,
            success: true,
            message: ''
          };
        }
        else {
          return {
            data: null,
            error: true,
            success: false,
            message: "Token is not valid!"
          };
        }
      }).catch(error => error);
    }
  }
};

/**
 *
 * @type {{gorm: (function(*): {loadModel: (function(*, *=): any), sequelize: (function(): *), close: close, connect: connect}), gauth: (function(*=, *=): {signIn: (function(*, *=): Promise<T | T>), profile: (function(*=, *=): Promise<T | T>), signUp: (function(*=): Promise<T | {data: null, success: boolean, error: boolean, message: *}>)})}}
 */
module.exports = {
  gorm,
  gauth
};
