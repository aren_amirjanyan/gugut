const CryptoJS = require("crypto-js"),
  moment = require("moment");

/**
 *
 * @type {{week: string, month: string, hour: string, year: string, millisecond: string, day: string, minute: string, second: string}}
 */
const expTimeDiff = {
  "year": "asYears",
  "month": "asMonths",
  "week": "asWeeks",
  "day": "asDays",
  "hour": "asHours",
  "minute": "asMinutes",
  "second": "asSeconds",
  "millisecond": "asMilliseconds",
};
/**
 *
 * @param config
 * @returns {{encrypt: (function(*, *=): string), verify: (function(*=, *=): boolean|boolean), decrypt: (function(*): any), isExpired: (function(*=): boolean)}}
 */
const gtoken = (config) => {
  return {
    /**
     * Encrypt data with expiration
     * @param signature
     * @param expiration
     * @returns {string}
     */
    encrypt: (signature, expiration = false) => {
      const exp = expiration ? expiration : moment().add(config.exp.value, config.exp.type);
      return CryptoJS.AES.encrypt(JSON.stringify({signature, exp}), config.secret).toString();
    },
    /**
     * Decrypt token
     * @param token
     * @returns {any}
     */
    decrypt: (token) => {
      return JSON.parse(CryptoJS.AES.decrypt(token.toString(), config.secret).toString(CryptoJS.enc.Utf8));
    },
    /**
     * Checked token is expired or no
     * @param token
     * @returns {boolean}
     */
    isExpired: (token) => {
      return moment.duration(moment(gtoken(config).decrypt(token).exp).diff(moment()))[expTimeDiff[config.exp.type]]() >= 0;
    },
    /**
     * Checked token is valid or no
     * @param token
     * @param signature
     * @returns {boolean}
     */
    verify: (token, signature) => {
      return JSON.stringify(gtoken(config).decrypt(token).signature) === JSON.stringify(signature) && gtoken(config).isExpired(token);
    }
  }
    ;
};
/**
 *
 * @type {{gtoken: (function(*): {encrypt: (function(*, *=): string), verify: (function(*=, *=): boolean|boolean), decrypt: (function(*): any), isExpired: (function(*=): boolean)})}}
 */
module.exports = {
  gtoken
};
