# gugut
Node.js token-based authentication module.

## Node.js (Install)

Requirements:

- Node.js
- npm (Node.js package manager)

```bash
npm install gugut
```
### Usage

Modular include:
```javascript
const {gorm, gauth} = require('./index');
const {gtoken} = require('./gtoken');
```

Create configs for gugut module
```javascript
const config = {
  database: 'gugut',
  username: 'root',
  password: '',
  dialect: 'mysql', /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  gtoken: {
    secret: 'hello my friend',
    exp: {
      value: 60000,
      type: 'millisecond', //"year","month","week","day","hour","minute","second","millisecond",
    }
  }
};
```

Load gugut ORM with config
```javascript
const gugutORM = gorm(config);
```
Load gugut gtoken module
```javascript
const gugutGtoken = gtoken(config.gtoken);
```
Create gugut Sequelize instance with
```javascript
const gugutSequelize = gugutORM.sequelize();
```
Connect DB with gugut ORM
```javascript
gugutORM.connection(gugutSequelize);
```
Create gugut Auth
```javascript
const gugutAuth = gauth(config, gugutSequelize);
```
Create custom data for sign up user
```javascript
const userData = {
  first_name: 'Abraham',
  last_name: 'Linkoln',
  email: 'abraham.linkoln@gmail.com',
  password: 'labla'
};
```
Sign up with custom user data
```javascript
gugutAuth.signUp(userData).then(user => {
  if (user.data) {
    //sign in with created user data
    gugutAuth.signIn(userData.email, userData.password).then(token => {
      if (token.data && gugutGtoken.isExpired(token.data)) {
        //get user data by user email and token
        gugutAuth.profile(userData.email, token.data).then(user => {
          if (user.data) {
            console.log(user.data);
          }
          else {
            console.error(user.message);
          }
        });
      }
      else {
        console.error(token.message);
      }
    });
  }
  else {
    console.error(user.message);
  }
});
```


