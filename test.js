const {gorm, gauth} = require('./index'),
  {gtoken} = require('./gtoken');

// create configs for gugut module
const config = {
  database: 'gugut',
  username: 'root',
  password: '',
  dialect: 'mysql', /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  gtoken: {
    secret: 'hello my friend',
    exp: {
      value: 60000,
      type: 'millisecond', //"year","month","week","day","hour","minute","second","millisecond",
    }
  }
};

//Load gugut ORM with config
const gugutORM = gorm(config);

// load gugut gtoken module
const gugutGtoken = gtoken(config.gtoken);

//create gugut Sequelize instance with
const gugutSequelize = gugutORM.sequelize();

// connect DB with gugut ORM
gugutORM.connection(gugutSequelize);

// create gugut Auth
const gugutAuth = gauth(config, gugutSequelize);

// create custom data for sign up user
const userData = {
  first_name: 'Abraham',
  last_name: 'Linkoln',
  email: 'abraham.linkoln@gmail.com',
  password: 'labla'
};

//sign up with custom user data
gugutAuth.signUp(userData).then(user => {
  if (user.data) {
    //sign in with created user data
    gugutAuth.signIn(userData.email, userData.password).then(token => {
      if (token.data && gugutGtoken.isExpired(token.data)) {
        //get user data by user email and token
        gugutAuth.profile(userData.email, token.data).then(user => {
          if (user.data) {
            console.log(user.data);
          }
          else {
            console.error(user.message);
          }
        });
      }
      else {
        console.error(token.message);
      }
    });
  }
  else {
    console.error(user.message);
  }
});

